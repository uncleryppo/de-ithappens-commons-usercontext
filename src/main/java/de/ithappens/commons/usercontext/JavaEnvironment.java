package de.ithappens.commons.usercontext;

/** 
 * Copyright: 2016 - 2017
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
*/
public class JavaEnvironment extends Environment {

    private String javaVersion, javaLocation, javaVendor, javaVendorUrl,
            javaClassPath;

    /**
     * Get java class path
     * @return java class path
     */
    public String getJavaClassPath() {
        return javaClassPath;
    }

    /**
     * Set java class path
     * @param javaClassPath java class path
     */
    public void setJavaClassPath(String javaClassPath) {
        this.javaClassPath = javaClassPath;
    }

    /**
     * Get java vendor
     * @return java vendor
     */
    public String getJavaVendor() {
        return javaVendor;
    }

    /**
     * Set java vendor
     * @param javaVendor java vendor
     */
    public void setJavaVendor(String javaVendor) {
        this.javaVendor = javaVendor;
    }

    /**
     * Get java vendor url
     * @return java vendor url
     */
    public String getJavaVendorUrl() {
        return javaVendorUrl;
    }

    /**
     * Set java vendor url
     * @param javaVendorUrl java vendor url
     */
    public void setJavaVendorUrl(String javaVendorUrl) {
        this.javaVendorUrl = javaVendorUrl;
    }

    /**
     * Get java version
     * @return java version
     */
    public String getJavaVersion() {
        return javaVersion;
    }

    /**
     * Set java version
     * @param javaVersion java version
     */
    public void setJavaVersion(String javaVersion) {
        this.javaVersion = javaVersion;
    }

    /**
     * Get java location
     * @return java location
     */
    public String getJavaLocation() {
        return javaLocation;
    }

    /**
     * Set java location
     * @param javaLocation java location
     */
    public void setJavaLocation(String javaLocation) {
        this.javaLocation = javaLocation;
    }
    
}
