package de.ithappens.commons.usercontext;

/** 
 * Copyright: 2016 - 2017
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 * @param <T>
*/
public abstract class Environment<T> {

    private T t;

    /**
     * Get environment
     * @return environment
     */
    public T get() {
        return this.t;
    }

    /**
     * Set environment
     * @param t environment
     */
    public void set(T t) {
        this.t = t;
    }

}
