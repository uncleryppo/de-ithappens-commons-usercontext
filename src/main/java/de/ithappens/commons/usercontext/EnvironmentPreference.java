package de.ithappens.commons.usercontext;

import java.math.BigDecimal;
import java.util.prefs.Preferences;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.validator.routines.BigDecimalValidator;

/** 
 * Copyright: 2016 - 2017
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
*/
public abstract class EnvironmentPreference extends Environment {
    
    private Preferences STORAGE;
    
    /**
     * Get the storage
     * @return preferences object of the current context
     */
    public Preferences getSTORAGE() {
        if (STORAGE == null) {
            if (getContext().equals(CONTEXT.USER)) {
                STORAGE = Preferences.userNodeForPackage(getClass());
            } else if (getContext().equals(CONTEXT.INSTALLATION)) {
                STORAGE = Preferences.systemNodeForPackage(getClass());
            }
        }
        return STORAGE;
    }

    /**
     * Context of the storage: User or Installation (=System)
     */
    public static enum CONTEXT{USER, INSTALLATION};
    
    /**
     * Get the context
     * @return context
     */
    public abstract CONTEXT getContext();
    
    public void setStringAndEncrypt(String key, String value) {
        if (value == null) {
            value = "";
        }
        STORAGE.putByteArray(key, value.getBytes());
    }
    
    /**
     * Get base64 encrypted property value, with alternative default value
     * @param key identifier for the value
     * @param defaultValue returned value if key does not exist
     * @return property value, base64 encrypted
     */
    public String getStringAndEncrypt(String key, String defaultValue) {
        byte[] byteArray = STORAGE.getByteArray(key, defaultValue != null ? defaultValue.getBytes() : null);
        return byteArray == null ? null : new String(byteArray);
    }
    
    /**
     * Set empty value
     * @param key identifier for the value
     */
    public void setEmpty(String key) {
        setString(key, "");
    }
    
    /**
     * Get property int value, with alternative default value
     * @param key identifier for the value
     * @param defaultValue returned value if key does not exist
     * @return property value, as int
     */
    public int get(String key, int defaultValue) {
        return getSTORAGE().getInt(key, defaultValue);
    }
    
    /**
     * Set property int value
     * @param key identifier for the value
     * @param value property value, as int
     */
    public void set(String key, int value) {
        getSTORAGE().putInt(key, value);
    }
    
    /**
     * Get property Integer value
     * @param key identifier for the value
     * @return property value, as Integer
     */
    public Integer getInteger(String key) {
        return getInteger(key, null);
    }
    
    /**
     * Get property Integer value, with alternative default value
     * @param key identifier for the value
     * @param defaultValue returned value if key does not exist
     * @return property value, as Integer
     */
    public Integer getInteger(String key, Integer defaultValue) {
        String value = getString(key);
        if (StringUtils.isEmpty(value)) {
            return defaultValue;
        }
        return NumberUtils.isNumber(value) ? Integer.parseInt(value) : null;
    }
    
    /**
     * Set property Integer value
     * @param key identifier for the value
     * @param value property value, as Integer
     */
    public void setInteger(String key, Integer value) {
        setString(key, value == null ? null : Integer.toString(value));
    }
    
    /**
     * Get property value, with alternative default value
     * @param key identifier for the value
     * @param defaultValue returned value if key does not exist
     * @return property value, as boolean
     */
    public boolean get(String key, boolean defaultValue) {
        return getSTORAGE().getBoolean(key, defaultValue);
    }
    
    /**
     * Set property boolean value
     * @param key identifier for the value
     * @param value property value, as boolean
     */
    public void set(String key, boolean value) {
        getSTORAGE().putBoolean(key, value);
    }
    
    /**
     * Get property Boolean value
     * @param key identifier for the value
     * @return value property value, as Boolean
     */
    public Boolean getBoolean(String key) {
        return getBoolean(key, null);
    }
    
    /**
     * Get property Boolean value, with alternative default value
     * @param key identifier for the value
     * @param defaultValue returned value if key does not exist
     * @return property value, as Boolean
     */
    public Boolean getBoolean(String key, Boolean defaultValue) {
        String value = getString(key);
        if (value == null) {
            return defaultValue;
        }
        return BooleanUtils.toBooleanObject(value);
    }
    
    /**
     * Set property Boolean value
     * @param key identifier for the value
     * @param value property value, as Boolean
     */
    public void setBoolean(String key, Boolean value) {
        setString(key, value == null ? null : Boolean.toString(value));
    }
    
    /**
     * Get property double value, with alternative default value
     * @param key identifier for the value
     * @param defaultValue returned value if key does not exist
     * @return property value, as double
     */
    public double get(String key, double defaultValue) {
        return getSTORAGE().getDouble(key, defaultValue);
    }
    
    /**
     * Set property double value
     * @param key identifier for the value
     * @param value property value, as double
     */
    public void set(String key, double value) {
        getSTORAGE().putDouble(key, value);
    }
    
    /**
     * Get property Double value
     * @param key identifier for the value
     * @return value property value, as Double
     */
    public Double getDouble(String key) {
        return getDouble(key, null);
    }
    
    /**
     * Get property Double value, with alternative default value
     * @param key identifier for the value
     * @param defaultValue returned value if key does not exist
     * @return property value, as Double
     */
    public Double getDouble(String key, Double defaultValue) {
        String value = getString(key);
        if (value == null) {
            return defaultValue;
        }
        return NumberUtils.isNumber(value) ? Double.parseDouble(value) : null;
    }
    
    /**
     * Set property Double value
     * @param key identifier for the value
     * @param value property value, as Double
     */
    public void setDouble(String key, Double value) {
        setString(key, value == null ? null : Double.toString(value));
    }
    
    /**
     * Get property long value, with alternative default value
     * @param key identifier for the value
     * @param defaultValue returned value if key does not exist
     * @return property value, as long
     */
    public long get(String key, long defaultValue) {
        return getSTORAGE().getLong(key, defaultValue);
    }
    
    /**
     * Set property long value
     * @param key identifier for the value
     * @param value property value, as long
     */
    public void set(String key, long value) {
        getSTORAGE().putLong(key, value);
    }
    
    /**
     * Get property Long value
     * @param key identifier for the value
     * @return value property value, as Long
     */
    public Long getLong(String key) {
        return getLong(key, null);
    }
    
    /**
     * Get property Long value, with alternative default value
     * @param key identifier for the value
     * @param defaultValue returned value if key does not exist
     * @return property value, as Long
     */
    public Long getLong(String key, Long defaultValue) {
        String value = getString(key);
        if (value == null) {
            return defaultValue;
        }
        return NumberUtils.isNumber(value) ? Long.parseLong(value) : null;
    }
    
    /**
     * Set property Long value
     * @param key identifier for the value
     * @param value property value, as Long
     */
    public void setLong(String key, Long value) {
        setString(key, value == null ? null : Long.toString(value));
    }
    
    /**
     * Get property float value, with alternative default value
     * @param key identifier for the value
     * @param defaultValue returned value if key does not exist
     * @return property value, as float
     */
    public float get(String key, float defaultValue) {
        return getSTORAGE().getFloat(key, defaultValue);
    }
    
    /**
     * Set property float value
     * @param key identifier for the value
     * @param value property value, as float
     */
    public void set(String key, float value) {
        getSTORAGE().putFloat(key, value);
    }
    
    /**
     * Get property Float value
     * @param key identifier for the value
     * @return value property value, as Float
     */
    public Float getFloat(String key) {
        return getFloat(key, null);
    }
    
    /**
     * Get property Float value, with alternative default value
     * @param key identifier for the value
     * @param defaultValue returned value if key does not exist
     * @return property value, as Float
     */
    public Float getFloat(String key, Float defaultValue) {
        String value = getString(key);
        if (value == null) {
            return defaultValue;
        }
        return NumberUtils.isNumber(value) ? Float.parseFloat(value) : null;
    }
    
    /**
     * Set property Float value
     * @param key identifier for the value
     * @param value property value, as Float
     */
    public void setFloat(String key, Float value) {
        setString(key, value == null ? null : Float.toString(value));
    }
    
    /**
     * Get property BigDecimal value
     * @param key identifier for the value
     * @return value property value, as BigDecimal
     */
    public BigDecimal getBigDecimal(String key) {
        return getBigDecimal(key, null);
    }
    
    /**
     * Get property BigDecimal value, with alternative default value
     * @param key identifier for the value
     * @param defaultValue returned value if key does not exist
     * @return property value, as BigDecimal
     */
    public BigDecimal getBigDecimal(String key, BigDecimal defaultValue) {
        String value = getString(key);
        if (value == null) {
            return defaultValue;
        }
        return BigDecimalValidator.getInstance().validate(value);
    }
    
    /**
     * Set property BigDecimal value
     * @param key identifier for the value
     * @param value property value, as BigDecimal
     */
    public void setBigDecimal(String key, BigDecimal value) {
        setString(key, value == null ? null : value.toString());
    }
    
    /**
     * Get property String value
     * @param key identifier for the value
     * @return value property value, as String
     */
    public String getString(String key) {
        return getString(key, null);
    }
    
    /**
     * Get property String value, with alternative default value
     * @param key identifier for the value
     * @param defaultValue returned value if key does not exist
     * @return property value, as String
     */
    public String getString(String key, Object defaultValue) {
        return getSTORAGE().get(key, defaultValue != null ? defaultValue.toString() : null);
    }
    
    /**
     * Set property String value
     * @param key identifier for the value
     * @param value property value, as String
     */
    public void setString(String key, String value) {
        if (value == null) {
            getSTORAGE().remove(key);
        } else {
            getSTORAGE().put(key, value);
        }
    }
    
}
