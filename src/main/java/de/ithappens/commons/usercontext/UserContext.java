package de.ithappens.commons.usercontext;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/** 
 * Copyright: 2016 - 2017
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
*/
public class UserContext {
    
    private static final UserContext SINGLETON = new UserContext();
    
    private final Map<Class, Environment> environments;
    
    private UserContext() {
        this.environments = new HashMap<>();
        environments.put(OperatingSystemEnvironment.class, createOperatingSystemEnvironment());
        environments.put(JavaEnvironment.class, createJavaEnvironment());
    }
    
    /**
     * Create OperatingSystemEnvironment object
     * @return operating system environment
     */
    public static OperatingSystemEnvironment createOperatingSystemEnvironment() {
        OperatingSystemEnvironment ose = new OperatingSystemEnvironment();
        ose.setSystemname(System.getProperty("os.name"));
        ose.setSystemversion(System.getProperty("os.version"));
        ose.setUsername(System.getProperty("user.name"));
        ose.setSystemarchitecture(System.getProperty("os.arch"));
        ose.setFileSeparator(System.getProperty("file.separator"));
        ose.setLineSeparator(System.getProperty("line.separator"));
        ose.setPathSeparator(System.getProperty("path.separator"));
        ose.setUserDir(System.getProperty("user.dir"));
        ose.setUserHome(System.getProperty("user.home"));
        ose.setSystemTempDirectory(System.getProperty("java.io.tmpdir"));
        
        return ose;
    }
    
    /**
     * Create JavaEnvironment object
     * @return java environment
     */
    public static JavaEnvironment createJavaEnvironment() {
        JavaEnvironment je = new JavaEnvironment();
        je.setJavaVersion(System.getProperty("java.version"));
        je.setJavaLocation(System.getProperty("java.home"));
        je.setJavaVendor(System.getProperty("java.vendor"));
        je.setJavaVendorUrl(System.getProperty("java.vendor.url"));
        je.setJavaClassPath(System.getProperty("java.class.path"));
        return je;
    }
    
    /**
     * Get UserContext singleton object
     * @return user context
     */
    public static UserContext getInstance() {
        return SINGLETON;
    }
    
    /**
     * Get map of all environments
     * @return all environments
     */
    public Map<Class, Environment> getEnvironments() {
        return environments;
    }
    
    /**
     * Add environment
     * @param environment environment to add
     */
    public void addEnvironment(Environment environment) {
        environments.put(environment.getClass(), environment);
    }
    
    /**
     * Get OperatingSystemEnvironment object
     * @return operating system environment
     */
    public OperatingSystemEnvironment getOperatingSystemEnvironment() {
        return getEnvironment(OperatingSystemEnvironment.class);
    }
    
    /**
     * Get JavaEnvironment object
     * @return java environment
     */
    public JavaEnvironment getJavaEnvironment() {
        return getEnvironment(JavaEnvironment.class);
    }
    
    /**
     * Get environment of class type T
     * @param <T> specific environment class type
     * @param c environment class
     * @return environment
     */
    public <T extends Environment> T getEnvironment(Class<T> c) {
        Environment env = environments.get(c);
        if (env != null) {
            return (T) env;
        } else {
            try {
                return c.newInstance();
            } catch (IllegalAccessException | InstantiationException ex) {
                Logger.getLogger(UserContext.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            } finally {
                return null;
            }
        }
    }

}

