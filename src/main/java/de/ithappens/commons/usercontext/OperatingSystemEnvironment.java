package de.ithappens.commons.usercontext;

/** 
 * Copyright: 2016 - 2017
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
*/
public class OperatingSystemEnvironment extends Environment {
    
    private String username, systemversion, systemname, systemarchitecture,
            fileSeparator, lineSeparator, pathSeparator, userDir, userHome,
            systemTempDirectory;

    /**
     * Get system temp directory
     * @return system temp directory
     */
    public String getSystemTempDirectory() {
        return systemTempDirectory;
    }

    /**
     * Set system temp directory
     * @param systemTempDirectory system temp directory
     */
    public void setSystemTempDirectory(String systemTempDirectory) {
        this.systemTempDirectory = systemTempDirectory;
    }

    /**
     * Get file separator
     * @return file separator
     */
    public String getFileSeparator() {
        return fileSeparator;
    }

    /**
     * Set file separator
     * @param fileSeparator file separator
     */
    public void setFileSeparator(String fileSeparator) {
        this.fileSeparator = fileSeparator;
    }

    /**
     * Get line separator
     * @return line separator
     */
    public String getLineSeparator() {
        return lineSeparator;
    }

    /**
     * Set line separator
     * @param lineSeparator line separator
     */
    public void setLineSeparator(String lineSeparator) {
        this.lineSeparator = lineSeparator;
    }

    /**
     * Get path separator
     * @return path separator
     */
    public String getPathSeparator() {
        return pathSeparator;
    }

    /**
     * Set path separator
     * @param pathSeparator path separator
     */
    public void setPathSeparator(String pathSeparator) {
        this.pathSeparator = pathSeparator;
    }

    /**
     * Get user directory
     * @return user directory
     */
    public String getUserDir() {
        return userDir;
    }

    /**
     * Set user directory
     * @param userDir user directory
     */
    public void setUserDir(String userDir) {
        this.userDir = userDir;
    }

    /**
     * Get user home
     * @return user home
     */
    public String getUserHome() {
        return userHome;
    }

    /**
     * Set user home
     * @param userHome user home
     */
    public void setUserHome(String userHome) {
        this.userHome = userHome;
    }

    /**
     * Get system architecture
     * @return system architecture
     */
    public String getSystemarchitecture() {
        return systemarchitecture;
    }

    /**
     * Set system architecture
     * @param systemarchitecture system architecture
     */
    public void setSystemarchitecture(String systemarchitecture) {
        this.systemarchitecture = systemarchitecture;
    }

    /**
     * Get user name
     * @return user name
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set user name
     * @param username user name
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get system version
     * @return system version
     */
    public String getSystemversion() {
        return systemversion;
    }

    /**
     * Set system version
     * @param systemversion system version
     */
    public void setSystemversion(String systemversion) {
        this.systemversion = systemversion;
    }

    /**
     * Get system name
     * @return system name
     */
    public String getSystemname() {
        return systemname;
    }

    /**
     * Set system name
     * @param systemname system name
     */
    public void setSystemname(String systemname) {
        this.systemname = systemname;
    }

}

